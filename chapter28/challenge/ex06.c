#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeWn(char *);
void clearStdin();

void printPrompt();

//Struct Info and functions

//Struct Info definition
typedef struct Info
{
	char name[20];
	char phonenum[15];
}Info;

Info getInfo(int, int);			//insert Info
void deleteInfo(Info*, char *, int); //delete Info
void searchInfo(Info*, char *, int);	//search Info
void printAllInfo(Info *, int);	//print Info

int main()
{
	int ch=0;	//choice
	int i=0;	//struct ptr
	int index=0;	//number of people saved
	char name[20];	//for searching and deleting

	//heap momory allocation for structure Info
	Info *ptr = (Info *)calloc(3, sizeof(Info));

	while(1)
	{
		printPrompt();		//print prompt
		scanf("%d", &ch);	//get choice
		clearStdin();		//clear stdin(remove '\n' in stdin)

		switch(ch)
		{
			//case 1: Insert
			case 1:
				ptr[i] = getInfo(sizeof((ptr+i)->name), sizeof((ptr+i)->phonenum));	//getInfo

				if( (i+1)%3 == 0 )	//when heap memory is full, reallocate
					ptr = (Info *)realloc(ptr, (i+4) * sizeof(Info));
				
				i++;	//ptr++;
				index++; //saved people--;

				break;

			//case 2: Delete
			case 2:
				fputs("Enter a name you want to delete : ", stdout);
				fgets(name, sizeof(name), stdin);
				removeWn(name);

				deleteInfo(ptr, name, index); 
				break;

			//case 3: Search
			case 3:
				fputs("Enter a name you want to search : ", stdout);
				fgets(name, sizeof(name), stdin);
				removeWn(name);

				searchInfo(ptr, name, index);
				break;

			//case 4: Print all
			case 4:
				printAllInfo(ptr, index);
				break;

			//case 5: Exit
			case 5:
				free(ptr);
				return 0;

			default:
				puts("Error : Wrong number");
				break;

		}	
		do{
			puts("\n\nIf you want to continue, press 'Enter'");
		}while(getchar()!='\n');
	}


}


void removeWn(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
		str[length-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar()!='\n');
}

void printPrompt()
{
	system("clear");
	//puts("\n\n");
	puts("---------- MENU ----------");
	puts("| 1. Insert              |");
	puts("| 2. Delete              |");
	puts("| 3. Search              |");
	puts("| 4. Print all           |");
	puts("| 5. Exit                |");
	puts("--------------------------");
	fputs("Choose num : ", stdout);
}

Info getInfo(int leng1, int leng2)
{
	Info temp;

	fputs("Enter a name : ", stdout);
	fgets(temp.name, leng1, stdin);
	removeWn(temp.name);

	fputs("Enter a phone number : ", stdout);
	fgets(temp.phonenum, leng2, stdin);
	removeWn(temp.phonenum);

	puts("\nData Insert completed!");

	return temp;
}

void deleteInfo(Info *ptr, char *name, int index) 	//delete Info
{
	for(int i=0; i<index; i++)
	{
		if(!strcmp((ptr+i)->name, name))
		{
			puts("\n[ Selected INFO is deleted ]");
			((ptr+i)->name)[0] = 0;
			return;
		}
	}

	puts("\nNo INFO having the name.");
}

void searchInfo(Info *ptr, char *name, int index)		//search Info
{
	for(int i=0; i<index; i++)
	{
		if(!strcmp((ptr+i)->name, name))
		{
			putchar('\n');
			puts("[ Searched INFO ]");
			printf("Name : %s	Tel : %s \n", (ptr+i)->name, (ptr+i)->phonenum);

			puts("\nSearching completed.");
			return;
		}
	}
		puts("\nNo INFO having the name.");
}


void printAllInfo(Info *ptr, int index)
{
	putchar('\n');
	puts("[ Print all INFO ]");

	for(int i=0; i<index; i++)
	{
		if(((ptr+i)->name)[0] == '\0')
			continue;
		
		printf("Name : %s	Tel : %s \n", (ptr+i)->name, (ptr+i)->phonenum);
	}
	puts("\nPrinting completed.");
}


	
