#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	FILE *fp = fopen(argv[1], "rt");
	int ch;
	int count_a=0, count_p=0;

	while(1)
	{
		ch = fgetc(fp); 
		if(ch == 'a' || ch == 'A')
			count_a++;

		else if(ch == 'p' || ch == 'P')
			count_p++;

		while(1)
		{
			ch = fgetc(fp);
			
			if(ch == ' ' || ch == '\n' || ch == '\t' || ch == EOF)
				break;
		}

		if(ch == EOF)
		{
			puts("File read is completed.");
			printf("Number of A words : %d \n", count_a);
			printf("Number of P words : %d \n", count_p);
			break;
		}
	}

	return 0;
}


