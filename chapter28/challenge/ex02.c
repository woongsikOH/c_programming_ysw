#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct BookInfo
{
	char writer[20];
	char subject[20];
	int pages;
}BookInfo;

void removeWn(char *);
void clearStdin();

int main()
{
	int i=0;
	char quit;

	BookInfo *ptr = (BookInfo *)calloc(3, sizeof(BookInfo));

	while(1)
	{
		printf("////////// Enter Book%d INFO //////////\n\n", i+1);
		//get writer's name
		fputs("Enter book writer's name : ", stdout);
		fgets((ptr+i)->writer, sizeof((ptr+i)->writer), stdin);
		removeWn((ptr+i)->writer);

		//get book name
		fputs("Enter book name : ", stdout);
		fgets((ptr+i)->subject, sizeof((ptr+i)->subject), stdin);
		removeWn((ptr+i)->subject);

		//get pages
		fputs("Enter total pages of book : ", stdout);
		scanf("%d", &(ptr+i)->pages);
		clearStdin();

		//query for continue
		puts("If you want to type another book INFO, press any key.");
		puts("(If you want to quit, press \'q\')");
		fputs(": ", stdout);

	       	//fputc(quit, stdin); clearStdin();
		//printf("\nFor debugging : %c \n", quit);
	       	scanf("%c", &quit); 
		if(quit != '\n')
			clearStdin();

		if(quit == 'q' || quit == 'Q')
			break;

		i++;

		//reallocate memory
		if(i%3 == 0)
			ptr = (BookInfo *)realloc(ptr, sizeof(BookInfo)*(i+3));
	}

	puts("\n\n////////// Print Book INFO //////////\n");
	for(int j=0; j<=i; j++)
	{
		printf("Book%d \n", j+1);
		printf("Writer : %s \n", (ptr+j)->writer);
		printf("Subject : %s \n", (ptr+j)->subject);
		printf("Pages : %d \n", (ptr+j)->pages);
		putchar('\n');
	}

	//free memory
	free(ptr);
	
	return 0;
}

void removeWn(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
		str[length-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar() != '\n');
}
