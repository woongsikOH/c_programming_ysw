#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeWn(char *);	//removeWn when get string.
void clearStdin();	//clear stdin

void printPrompt();	//print menu on the screen

//Struct Info and functions
typedef struct Info	//Struct Info definition
{
	char name[20];
	char phonenum[15];
}Info;

Info getInfo(int, int);			//insert Info
void deleteInfo(Info*, char *, int); 	//delete Info
void searchInfo(Info*, char *, int);	//search Info
void printAllInfo(Info *, int);		//print Info

//save and load
void saveInfo(char *, Info *, int);
void loadInfo(char *, Info *, int *);

int main(int argc, char *argv[])
{

	//if filename is not entered.
	if(argc < 2)
	{
		puts("Error : ex07 must be executed with txt file.");
		puts("please Enter the command in this form [./ex07 filename.txt]");

		return -1;
	}


	int ch=0;	//choice
	int i=0;	//struct ptr
	int index=0;	//number of people saved
	char name[20];	//for searching and deleting

	Info *ptr = (Info *)calloc(3, sizeof(Info));	//heap momory allocation for structure Info
	
	loadInfo(argv[1], ptr, &index);	//load data if there are.
	i = index;

	puts("in main function.");
	for(int j=0; j<index; j++){
		printf("(ptr+%d) contents \n", j);
		printf("%s %s \n", (ptr+j)->name, (ptr+j)->phonenum);
		//printf("index : %d \n", *index);
	}

	while(1)
	{
		printPrompt();		//print prompt
		scanf("%d", &ch);	//get choice
		clearStdin();		//clear stdin(remove '\n' in stdin)

		switch(ch)
		{
			//case 1: Insert
			case 1:
				ptr[i] = getInfo(sizeof((ptr+i)->name), sizeof((ptr+i)->phonenum));	//getInfo

				if( (i+1)%3 == 0 )	//when heap memory is full, reallocate
					ptr = (Info *)realloc(ptr, (i+4) * sizeof(Info));
				
				i++;	//ptr++;
				index++; //saved people--;

				break;

			//case 2: Delete
			case 2:
				fputs("Enter a name you want to delete : ", stdout);
				fgets(name, sizeof(name), stdin);
				removeWn(name);

				deleteInfo(ptr, name, index); 
				break;

			//case 3: Search
			case 3:
				fputs("Enter a name you want to search : ", stdout);
				fgets(name, sizeof(name), stdin);
				removeWn(name);

				searchInfo(ptr, name, index);
				break;

			//case 4: Print all
			case 4:
				printAllInfo(ptr, index);
				break;

			//case 5: Exit
			case 5:
				//saving process
				saveInfo(argv[1], ptr, index);

				free(ptr);
				return 0;

			default:
				puts("Error : Wrong number");
				break;

		}	
		do{
			puts("\n\nIf you want to continue, press 'Enter'");
		}while(getchar()!='\n');
	}


}


void removeWn(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
		str[length-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar()!='\n');
}

void printPrompt()
{
	//system("clear");	//console clearing
	
	puts("---------- MENU ----------");
	puts("| 1. Insert              |");
	puts("| 2. Delete              |");
	puts("| 3. Search              |");
	puts("| 4. Print all           |");
	puts("| 5. Exit                |");
	puts("--------------------------");
	fputs("Choose num : ", stdout);
}

Info getInfo(int leng1, int leng2)
{
	Info temp;

	fputs("Enter a name : ", stdout);
	fgets(temp.name, leng1, stdin);
	removeWn(temp.name);

	fputs("Enter a phone number : ", stdout);
	fgets(temp.phonenum, leng2, stdin);
	removeWn(temp.phonenum);

	puts("\nData Insert completed!");

	return temp;
}

void deleteInfo(Info *ptr, char *name, int index) 	//delete Info
{
	for(int i=0; i<index; i++)
	{
		if(!strcmp((ptr+i)->name, name))
		{
			puts("\n[ Selected INFO is deleted ]");
			((ptr+i)->name)[0] = 0;
			return;
		}
	}

	puts("\nNo INFO having the name.");
}

void searchInfo(Info *ptr, char *name, int index)
{
	for(int i=0; i<index; i++)
	{
		if(!strcmp((ptr+i)->name, name))
		{
			putchar('\n');
			puts("[ Searched INFO ]");
			printf("Name : %s	Tel : %s \n", (ptr+i)->name, (ptr+i)->phonenum);

			puts("\nSearching completed.");
			return;
		}
	}
		puts("\nNo INFO having the name.");
}


void printAllInfo(Info *ptr, int index)
{
	putchar('\n');
	puts("[ Print all INFO ]");

	for(int i=0; i<index; i++)
	{
		if(((ptr+i)->name)[0] == '\0')
			continue;
		
		printf("Name : %s	Tel : %s \n", (ptr+i)->name, (ptr+i)->phonenum);
	}
	puts("\nPrinting completed.");
}

void saveInfo(char *filename, Info *ptr, int index)
{
	FILE *fp = fopen(filename, "wt");

	for(int i=0; i<index; i++)
	{
		if(((ptr+i)->name)[0] == '\0')
			continue;
		
		fprintf(fp, "%s %s \n", (ptr+i)->name, (ptr+i)->phonenum);
	}

	fclose(fp);

	/*
	do{
		puts("\n\nData saved safely. press any key");
	}while(getchar() > 127);
	*/
}


void loadInfo(char *filename, Info *ptr, int *index)
{
	int ret;
	int i=0;

	FILE *fp = fopen(filename, "rt");
	if(fp==NULL)
	{
		return;
	}

	while(1)
	{
		ret = fscanf(fp, "%s %s", (ptr+i)->name, (ptr+i)->phonenum);

		if(ret == EOF)
			break;

		//for debugging
		printf("(ptr+%d) contents \n", i);
		printf("%s %s \n", (ptr+i)->name, (ptr+i)->phonenum);
		printf("index : %d \n\n", *index);
		

		while(fgetc(fp) != '\n');
		//fseek(fp, 1, SEEK_CUR);


		i++;
		(*index)++;

		if( (i+1)%3 == 0 )	//when heap memory is full, reallocate
			ptr = (Info *)realloc(ptr, (i+4) * sizeof(Info));

	}

	/fclose(fp);
}
