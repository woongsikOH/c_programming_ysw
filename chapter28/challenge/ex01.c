#include <stdio.h>
#include <string.h>

typedef struct BookInfo
{
	char writer[20];
	char subject[20];
	int pages;
}BookInfo;

void removeWn(char *);
void clearStdin();

int main()
{
	BookInfo arr[3];

	for(int i=0; i<3; i++)
	{
		printf("////////// INPUT BOOK%d INFO //////////\n", i+1);
		
		fputs("Enter writer's name : ", stdout);
		fgets(arr[i].writer, sizeof(arr[i].writer), stdin);
	        removeWn(arr[i].writer);

		fputs("Enter book name : ", stdout);
		fgets(arr[i].subject, sizeof(arr[i].subject), stdin);
	        removeWn(arr[i].subject);

		fputs("Enter total pages of book  : ", stdout);
		scanf("%d", &arr[i].pages);
		clearStdin();
	}

	puts("\n\n////////// Print Book INFO //////////\n");
	for(int i=0; i<3; i++)
	{
		printf("Book%d \n", i+1);
		printf("Writer : %s \n", arr[i].writer);
		printf("Subject : %s \n", arr[i].subject);
		printf("Pages : %d \n", arr[i].pages);
		putchar('\n');
	}

	return 0;
}

void removeWn(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
		str[length-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar() != '\n');
}
