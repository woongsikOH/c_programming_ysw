#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE *fp1 = fopen(argv[1], "rt");
	FILE *fp2 = fopen(argv[2], "rt");

	int ch1=0, ch2=0;

	while(1)
	{
		ch1 = fgetc(fp1);
		ch2 = fgetc(fp2);

		if(ch1 != ch2)
		{
			puts("The two text files are different!");

			break;
		}

		else if(ch1 == EOF)
		{
			puts("The two files are same!");

			break;
		}
	}
			
	return 0;
}
