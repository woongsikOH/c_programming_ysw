#include <stdio.h>

int main()
{
	double rad;
	double area;
	printf("input radius : ");
	scanf("%lf", &rad);

	area = rad*rad*3.1415;
	
	printf("Area of circle : %f \n", area);

	return 0;
}
