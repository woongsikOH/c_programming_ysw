#include <stdio.h>

int main()
{
	char num1 = -5;

	printf("size of data type 'char' : %lu \n", sizeof(num1));
	printf("num1 : %d \n", num1);

	return 0;
}
