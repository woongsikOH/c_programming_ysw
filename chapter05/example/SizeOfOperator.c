#include <stdio.h>

int main()
{
	char ch=9;
	int inum = 1052;
	double dnum = 3.1415;
	printf("size of variable ch : %lu \n", sizeof(ch));
	printf("size of variable inum : %lu \n", sizeof(inum));
	printf("size of variable dnum : %lu \n", sizeof(dnum));
	printf("\n");

	printf("size of datatype 'char' : %lu \n", sizeof(char));
	printf("size of datatype 'short' : %lu \n", sizeof(short));
	printf("size of datatype 'int' : %lu \n", sizeof(int));
	printf("size of datatype 'long' : %lu \n", sizeof(long));
	printf("size of datatype 'long long' : %lu \n", sizeof(long long));
	printf("size of datatype 'float' : %lu \n", sizeof(float));
	printf("size of datatype 'double' : %lu \n", sizeof(double));

	return 0;
}
