#include <stdio.h>

int main()
{
	signed char num = 127;

	printf("%d, %x \n",num, num);
	printf("%d, %x \n",~num, ~num);
	printf("%d \n", ~num + ~num);

	return 0;
}
