#include <stdio.h>

int main()
{
	char ascii=0;

	printf("Input a decimal integer(0~127) : ");
	scanf("%d", (int*)&ascii);
	printf("The input number's ASCII code : %c \n", ascii);

	return 0;
}
