#include <stdio.h>

int main()
{
	double num1 = 245;
	int num2 = 3.1415;
	int num3 = 129;
	char ch = num3;

	printf("integer 245 to real number : %f \n", num1);
	printf("real number 3.1415 to integer : %d \n", num2);
	printf("auto data type cast (big -> small) : %d \n", ch);

	return 0;
}
