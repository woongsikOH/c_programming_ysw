#include <stdio.h>

int main()
{
	double num1=0, num2=0;

	printf("Input two floating number : ");
	scanf("%lf %lf", &num1, &num2);

	printf("\n");
	printf("-----------------------------------------------------------\n");
	printf("The Summation of two number : %f \n", num1+num2);
	printf("The Subtraction of two number : %f \n", num1-num2);
	printf("The Multiplication of two number : %f \n", num1*num2);
	printf("The Dividing of two number : %f \n", num1/num2);
	printf("-----------------------------------------------------------\n");

	return 0;
}
