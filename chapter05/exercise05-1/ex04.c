#include <stdio.h>

int main()
{
	int ascii=0;

	printf("Input a decimal integer(0~127) : ");
	scanf("%d", &ascii);
	printf("The input number's ASCII code : %c \n", ascii);

	return 0;
}
