#include <stdio.h>

int main()
{
	int x1=0, x2=0, y1=0, y2=0;

	printf("input cordination x1, y1 : ");
	scanf("%d %d", &x1, &y1);
	printf("\n");

	printf("input cordination x2, y2 : ");
	scanf("%d %d", &x2, &y2);
	printf("\n");

	printf("The area of the rectangle : %d \n", (x2-x1) * (y2-y1));

	return 0;
}
