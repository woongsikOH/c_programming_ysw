#include <stdio.h>

void showDemiacalToBinary(int);
void changeDemicalToBinary(int, char*);

int main()
{
	int num;
	char binary[100]={0};

	printf("Enter a demical integer : ");
	scanf("%d", &num);

	showDemiacalToBinary(num);
	changeDemicalToBinary(num, &binary[0]);

	/*
	//for test
	printf("char binary[100] in main : ");
	for(int i=0; i<10; i++)
		printf("%d", binary[9-i]);
	printf("\n");

	changeDemicalToBinary(num, &binary[0]);

	//for test
	printf("char binary[100] in main : ");
	for(int i=0; i<10; i++)
		printf("%d", binary[9-i]);
	printf("\n");
	*/

	return 0;
}

void showDemiacalToBinary(int num)
{
	char binary[100]={0};
	int i=0;

	while(1)
	{
		binary[i] = num%2;
		i++;	//i is the length of binary number

		if(num == 1)
		{
			break;
		}
		else
		{
			num/=2;
		}
	}
	
	for(int j=0; j<i; j++)
		printf("%d", binary[i-1-j]);

	printf("\n");
}

void changeDemicalToBinary(int num, char *binary) 
{
	int i=0;

	while(1)
	{
		binary[i] = num%2;
		i++;	//i is the length of binary number

		if(num == 1)
		{
			break;
		}
		else
		{
			num/=2;
		}
	}
	
	for(int j=0; j<i; j++)
		printf("%d", binary[i-1-j]);

	printf("\n");
}
