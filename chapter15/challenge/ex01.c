#include <stdio.h>

void showOddNum(int *, int);
void showEvenNum(int *, int);

int main()
{
	int num[10]={0};
	printf("Enter 10 integers with space : ");

	for(int i=0; i<10; i++)
		scanf("%d", &num[i]);

	showOddNum(&num[0], sizeof(num)/sizeof(int));
	showEvenNum(&num[0], sizeof(num)/sizeof(int));

	return 0;
}

void showOddNum(int *arr, int length)
{
	printf("Odd numbers : ");

	for(int i=0; i<length; i++)
	{
		if(arr[i]%2 == 1)
			printf("%d ", arr[i]);
	}

	printf("\n");
}

void showEvenNum(int *arr, int length)
{
	printf("Even numbers : ");

	for(int i=0; i<length; i++)
	{
		if(*(arr+i)%2 == 0)
			printf("%d ", *(arr+i));
	}

	printf("\n");
}
