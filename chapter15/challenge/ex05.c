#include  <stdio.h>

void descendingSort(int *, int);

int main()
{
	int arr[7]={0};


	for(int i=0; i<7; i++)
	{
		printf("Enter a integer : ");
		scanf("%d", &arr[i]);
	}
	
	descendingSort(&arr[0], 7);

	for(int i=0; i<7; i++)
		printf("%d ", arr[i]);

	printf("\n");

	return 0;
}

void descendingSort(int *arr, int length)
{
	int temp=0;
	int i=0;

	while(i < length)
	{
		while(i != length-1)
		{
			if(*(arr+i) < *(arr+1+i))
			{
				temp = *(arr+1+i);
				*(arr+1+i) = *(arr+i);
				*(arr+i) = temp;
			}

			i++;
		}

		i=0;
		length--;
	}
}
