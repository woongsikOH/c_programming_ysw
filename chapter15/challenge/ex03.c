#include <stdio.h>

int main()
{
	int ary[10]={0};
	int sort_ary[10]={0};

	int *front = &sort_ary[0];
	int *back = &sort_ary[9];


	for(int i=0; i<10; i++)
	{
		printf("Enter a integer : ");
		scanf("%d", &ary[i]);

		if(ary[i]%2 == 1)	//when odd
		{
			*front=ary[i];
			front++;
		}
		else	//when even
		{
			*back=ary[i];
			back--;
		}
	}

	for(int i=0; i<10; i++)
		printf("%d ", sort_ary[i]);

	printf("\n");

	return 0;
}
