#include <stdio.h>

int strlength(char *);
void isPalindrome(char *, int);

int main()
{
	char word[100]={0};
	int length=0;

	printf("Enter a word : ");
	scanf("%s", word);

	//define length of word
	length=strlength(&word[0]);
	
	isPalindrome(&word[0], length);

	return 0;
}

int strlength(char *word)
{
	int i=0;

	while(word[i] != '\0')
	{
		i++;
	}

	return i;
}

void isPalindrome(char *word, int length)
{
	char *front = &word[0];
	char *back = &word[length-1];

	for(int i=0; i<length/2; i++)	
	{
		if(*front != *back)
		{
			printf("It's not a Palindrome.\n");
			return;
		}

		front++;
		back--;
	}

	printf("It's a Palindrome.\n");
}
