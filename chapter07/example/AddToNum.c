#include <stdio.h>

int main()
{
	int total=0;
	int num;

	printf("Summation from 0 to num, enter the num : ");
	scanf("%d", &num);

	for(int i=0; i<num+1; i++){
		total += i;
	}

	printf("Summation from 0 to the num : %d \n", total);

	return 0;
}
