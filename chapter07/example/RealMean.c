#include <stdio.h>

int main()
{
	double total=0.0, input=0.0;
	int num=0;

	for( ; input>=0.0;){
		total += input;
		printf("Enter a real num (minus to quit) : ");
		scanf("%lf", &input);

		num++;
	}

	printf("Average of input numbers : %f \n", total/(num-1));

	return 0;
}
