#include <stdio.h>

int main()
{
	int i=0, num=0, result=0;

	while(i < 3){
		num=0;

		while(num < 1){
			printf("Enter positive integer same or over 1 : ");
			scanf("%d", &num);
		}
		
		result += num;

		i++;
	}
	
	printf("The summation of the input numbers : %d \n", result);

	return 0;
}
