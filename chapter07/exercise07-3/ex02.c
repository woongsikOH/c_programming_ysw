#include <stdio.h>

int main()
{
	int total=0, i=1;

	do{
		while(i%2 != 0)
			i++;

		total += i;
		i++;
	}while(i<101);

	printf("Summation of even number less than 100 : %d\n", total);

	return 0;
}
