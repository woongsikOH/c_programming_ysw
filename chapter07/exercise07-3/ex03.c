#include <stdio.h>

int main()
{
	int i=2;
	int j=0;

	do{
		j=1;
		do{
			printf("%d X %d = %d \n", i, j, i*j);
			j++;
		}while(j<10);

		printf("\n");
		i++;
	}while(i<10);

	return 0;
}
