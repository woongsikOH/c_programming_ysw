#include <stdio.h>

int main()
{
	int i=0, num=0; 
	double temp=0, average=0;
	printf("Enter a number of numbers you want to make a average : ");
	scanf("%d", &num);

	while(i < num){
		printf("Enter a number that you want to make a average : ");
		scanf("%lf", &temp);
		average += temp;
		
		i++;
	}

	average /= num;
	printf("\nAverage : %f \n", average);

	return 0;
}	
