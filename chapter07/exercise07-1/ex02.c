#include <stdio.h>

int main()
{
	int num=0, i=0;

	printf("Please Enter a positive integer in demical : ");
	scanf("%d", &num);

	while(i<num){
		printf("3 X %d = %d \n", i+1, (i+1)*3);
		i++;
	}

	return 0;
}
