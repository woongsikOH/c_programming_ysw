#include <stdio.h>

int main()
{
	int dan=0, num=1;
	printf("몇 단? : ");
	scanf("%d", &dan);

	while(num < 10){
		printf("%d X %d = %d \n", dan, 10-num, dan*(10-num));
		num++;
	}

	return 0;
}
