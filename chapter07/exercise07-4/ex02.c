#include <stdio.h>

int main()
{
	int num=0, result=1;

	printf("Enter a integer to make a factorial : ");
	scanf("%d", &num);

	/*
	//using for loop
	for(int i=1; i<num+1; i++){
		result *= i;
	}
	*/

	/*
	// using do while loop
	int i=1;
	do{
		result *= i;
		i++;
	}while(i <= num);
	*/

	int i=1;
	while(i <= num){
		result *= i;
		i++;
	}

	printf("The result : %d \n", result);

	return 0;
}
