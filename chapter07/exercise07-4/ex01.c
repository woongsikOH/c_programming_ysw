#include <stdio.h>

int main()
{
	int i1=0, i2=0, total=0;

	printf("Enter a two integer in demical (i1 i2) : ");
	scanf("%d %d", &i1, &i2);

	/*
	//using 'for'
	for(int i=i1; i<=i2; i++){
		total += i;
	}
	*/

	/*
	//using 'do while'
	do{
		total += i1;
		i1++;
	}while(i1 <= i2);
	*/

	while(i1 <= i2){
		total += i1;
		i1++;
	}

	printf("The summation between input numbers i1, i2 : %d \n", total);		
	return 0;
}
