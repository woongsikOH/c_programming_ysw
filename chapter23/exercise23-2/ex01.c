#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

typedef struct Rectangle
{
	Point top_left;
	Point bottom_right;
}Rectangle;

void areaOfRectangle(Rectangle *r1)
{
	int area = 0;
	area = (r1->top_left.xpos - r1->bottom_right.xpos) * (r1->top_left.ypos - r1->bottom_right.ypos);
	area = area > 0 ? area:area*-1;	

	printf("Area : %d \n", area);
}

void show4PointsOfRectangle(Rectangle *r1)
{
	puts("Print four points of Rectangle");
	printf("(x1, y1) : (%d, %d) \n", r1->top_left.xpos, r1->top_left.ypos);
	printf("(x2, y2) : (%d, %d) \n", r1->bottom_right.xpos, r1->top_left.ypos);
	printf("(x3, y3) : (%d, %d) \n", r1->bottom_right.xpos, r1->bottom_right.ypos);
	printf("(x4, y4) : (%d, %d) \n", r1->top_left.xpos, r1->bottom_right.ypos);
	putchar('\n');
}


int main()
{
	Rectangle r1={{0,0}, {100,100}};

	show4PointsOfRectangle(&r1);
	areaOfRectangle(&r1);

	return 0;
}


