#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

void swapPoint(Point *, Point *);
void showPoint(Point);

int main()
{
	Point pos1 = {2, 4};
	Point pos2 = {5, 7};

	puts("Before swap.");
	showPoint(pos1);
	showPoint(pos2);

	swapPoint(&pos1, &pos2);
	puts("After swap.");
	showPoint(pos1);
	showPoint(pos2);

	return 0;
}

void swapPoint(Point *pos1, Point *pos2)
{
	Point temp;

	temp.xpos = pos1->xpos;
	temp.ypos = pos1->ypos;

	pos1->xpos = pos2->xpos;
	pos1->ypos = pos2->ypos;

	pos2->xpos = temp.xpos;
	pos2->ypos = temp.ypos;
}

void showPoint(Point pos)
{
	printf("[%d, %d] \n", pos.xpos, pos.ypos);
}

