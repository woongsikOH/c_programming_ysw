#include <stdio.h>

typedef struct Sbox
{
	int mem1;
	int mem2;
	double mem3;
}Sbox;

typedef union Ubox
{
	int mem1;
	int mem2;
	double mem3;
}Ubox;

int main()
{
	Sbox sbx;
	Ubox ubx;

	printf("%p %p %p \n", &sbx.mem1, &sbx.mem2, &sbx.mem3);
	printf("%p %p %p \n", &ubx.mem1, &ubx.mem2, &ubx.mem3);
	printf("%lu %lu \n", sizeof(Sbox), sizeof(Ubox));	

	return 0;
}
