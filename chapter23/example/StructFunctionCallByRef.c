#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

void orgSymTrans(Point *ptr)	//원점 대칭
{
	ptr->xpos = (ptr->xpos) * -1;
	ptr->ypos = (ptr->ypos) * -1;
}

void showPosition(Point pos)
{
	printf("[%d, %d] \n", pos.xpos, pos.ypos);
}

int main()
{
	Point pos={7, -5};

	orgSymTrans(&pos);
	showPosition(pos);

	orgSymTrans(&pos);
	showPosition(pos);

	return 0;
}
