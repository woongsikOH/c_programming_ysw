#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

Point addPoint(Point pos1, Point pos2)
{
	pos1.xpos += pos2.xpos;
	pos1.ypos += pos2.ypos;

	return pos1;
}

Point minPoint(Point pos1, Point pos2)
{
	pos1.xpos -= pos2.xpos;
	pos1.ypos -= pos2.ypos;

	return pos1;
}

int main()
{
	Point pos1 = {5,6};
	Point pos2 = {2,9};

	//add
	Point result = addPoint(pos1, pos2);
	printf("[%d, %d] \n", result.xpos, result.ypos);

	//add
	result = minPoint(pos1, pos2);
	printf("[%d, %d] \n", result.xpos, result.ypos);

	return 0;
}
