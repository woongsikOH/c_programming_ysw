#include <stdio.h>

typedef struct Person
{
	char name[20];
	char phoneNum[20];
	int age;
}Person;

void showPersonInfo(Person man)
{
	printf("name : %s \n", man.name);
	printf("phone : %s \n", man.phoneNum);
	printf("age : %d \n", man.age);
}

Person writePersonInfo()
{
	Person man;
	fputs("Enter a name : ", stdout); scanf("%s", man.name);
	fputs("Enter a phone number : ", stdout); scanf("%s", man.phoneNum);
	fputs("Enter a age : ", stdout); scanf("%d", &man.age);

	return man;
}

int main()
{
	Person man = writePersonInfo();
	showPersonInfo(man);

	return 0;
}
