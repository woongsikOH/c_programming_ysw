#include <stdio.h>

typedef struct Student
{
	char name[20];
	char stdnum[20];
	char school[20];
	char major[20];
	int year;
}Student;

void showStudentInfo(Student *sptr)
{
	printf("Name : %s \n", sptr->name);
	printf("StuNum : %s \n", sptr->stdnum);
	printf("School : %s \n", sptr->school);
	printf("Major : %s \n", sptr->major);
	printf("Year : %d \n", sptr->year);
}

int main()
{
	Student arr[7];

	for(int i=0; i<7; i++)
	{
		printf("Name : "); scanf("%s", arr[i].name);
		printf("StuNum : "); scanf("%s", arr[i].stdnum);
		printf("School : "); scanf("%s", arr[i].school);
		printf("Major : "); scanf("%s", arr[i].major);
		printf("Year : "); scanf("%d", &arr[i].year);
	}

	for(int i=0; i<7; i++)
		showStudentInfo(&arr[i]);

	return 0;
}

