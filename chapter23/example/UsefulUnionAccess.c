#include <stdio.h>

typedef struct DBShort
{
	unsigned short upper;
	unsigned short lower;
}DBShort;

typedef struct RDBuf
{
	int iBuf;
	char bBuf[4];
	DBShort sBuf;
}RDBuf;

int main()
{
	RDBuf buf;

	fputs("Enter a integer : ", stdout);
	scanf("%d", &(buf.iBuf));

	printf("Upper 2bytes : %u \n", buf.sBuf.upper);
	printf("Lower 2bytes : %u \n", buf.sBuf.lower);
	printf("Upper 1byte ASCII : %c \n", buf.bBuf[0]);
	printf("Lower 1byte ASCII : %c \n", buf.bBuf[3]);

	return 0;
}
