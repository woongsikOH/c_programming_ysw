#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

void showPostion(Point p1)
{
	printf("[%d, %d] \n", p1.xpos, p1.ypos);
}

Point getCurrentPosition()
{
	Point p1;

	fputs("Enter current postion : ", stdout);
	scanf("%d %d", &p1.xpos, &p1.ypos);
	
	return p1;
}	

int main()
{
	Point curPos = getCurrentPosition();
	showPostion(curPos);

	return 0;
}
