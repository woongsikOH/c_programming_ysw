#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
};

typedef struct point Point;

typedef struct person
{
	char name[20];
	char phoneNum[20];
	int age;
}Person;

int main()
{
	Point point1={10, 20};
	Person person1={"Sheldon Cooper", "034-123-12", 25};

	printf("%d %d \n", point1.xpos, point1.ypos);
	printf("%s %s %d \n", person1.name, person1.phoneNum, person1.age);

	return 0;
}
