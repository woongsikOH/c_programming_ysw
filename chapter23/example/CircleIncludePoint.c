#include <stdio.h>

typedef struct Point
{
	int xpos;
	int ypos;
}Point;

typedef struct Circle
{
	Point center;
	double radius;
}Circle;

void showCircleInfo(Circle *ptr)
{
	printf("The center : [%d, %d] \n", ptr->center.xpos, ptr->center.ypos);
	printf("The radius : %f \n", ptr->radius);
}

int main()
{
	Circle c1 = {{1,2}, 3.5};
	Circle c2 = {2, 4, 3.9};

	showCircleInfo(&c1);
	showCircleInfo(&c2);

	return 0;
}
