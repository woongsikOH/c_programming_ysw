#include <stdio.h>

int main()
{
	int num=10;
	int *ptr1 = &num;
	int *ptr2 = ptr1;
	//int *ptr2 = *ptr1;	//error. *ptr1은 num에 저장된 int 숫자이기 때문에(주소값이 아니므로)

	(*ptr1)++;
	(*ptr2)++;

	printf("%d \n", num);

	return 0;
}
