#include <stdio.h>

int main()
{
	FILE *src = fopen("src.bin", "rb");
	FILE *dst = fopen("dst.bin", "wb");

	char buf[20];
	int readCnt;

	if(src == NULL || dst == NULL){
		puts("File open failed");
		return -1;
	}

	while(1)
	{
		readCnt=fread((void *)buf, 1, sizeof(buf), src);

		if(readCnt<sizeof(buf))
		{
			if(feof(src)!=0)
			{
				fwrite((void*)buf, 1, readCnt, dst);
				puts("File copy completed");
				break;
			}
			else
				puts("File copy failed");

			break;
		}

		fwrite((void*)buf, 1, sizeof(buf), dst);
	}

	fclose(src);
	fclose(dst);

	return 0;
}
