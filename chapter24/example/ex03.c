#include <stdio.h>

int main()
{
	FILE *fp = fopen("mystory.txt", "rt");
	if(fp == NULL){
		puts("File open error");
		return -1;
	}

	//copy char by char
	/*
	 *
	int ch;

	while(ch=fgetc(fp) != EOF)
		fputc(ch, stdout);

	if(feof(fp) == 0)
		puts("Copy has failed");
	*
	*/

	//copy str by str
	
	char str[20];

	while(fgets(str, sizeof(str), fp) != NULL)
		fputs(str, stdout);

	if(feof(fp) == 0)
		puts("Copy has failed");

	fclose(fp);

	return 0;
}
