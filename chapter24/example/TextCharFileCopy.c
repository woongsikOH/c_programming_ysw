#include <stdio.h>

int main()
{
	FILE *src=fopen("src.txt", "rt");
	FILE *dst=fopen("dst.txt", "wt");
	int ch;

	if(src==NULL || dst==NULL){
		puts("File open failed");
		return -1;
	}

	while((ch=fgetc(src))!=EOF)
		fputc(ch, dst);

	if(feof(src)!=0)
		puts("File copy is completed!");
	else
		puts("File copy is failed.");

	fclose(src);
	fclose(dst);

	return 0;
}
