#include <stdio.h>

int main()
{
	FILE *fp = fopen("mystory.txt", "wt");
	if(fp == NULL)
	{
		puts("File open failed");
		return -1;
	}

	fputs("#이름 : 윤성우\n", fp);
	fputs("#주민번호 : 900208-1012589\n", fp);
	fputs("#전화번호 : 010-1111-2222\n", fp);

	return 0;
}
