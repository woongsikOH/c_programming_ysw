#include <stdio.h>

int main()
{
	//file open(text write mode)
	FILE *fp=fopen("text.txt", "wt");
	fputs("123456789", fp);
	fclose(fp);

	//file open(text read mode)
	fp=fopen("text.txt", "rt");

	fseek(fp, -2, SEEK_END);
	putchar(fgetc(fp));

	fseek(fp, 2, SEEK_SET);
	putchar(fgetc(fp));

	fseek(fp, 2, SEEK_CUR);
	putchar(fgetc(fp));

	putchar('\n');

	fclose(fp);
	return 0;
}
