#include <stdio.h>

int main()
{
	char name[10];
	char sex;
	int age;

	FILE *fp = fopen("friend.txt", "wt");
	
	for(int i=0; i<3; i++)
	{
		fputs("이름 성별 나이 입력 : ", stdout);
		scanf("%s %c %d", name, &sex, &age);
		fprintf(fp, "%s %c %d", name, sex, age);
	}

	fclose(fp);

	return 0;
}
