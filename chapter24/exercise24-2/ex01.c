#include <stdio.h>

void printFileSize(FILE *);

int main()
{

	FILE *fp = fopen("text.txt", "wt");

	
	fputs("I've never seen the beautiful like her.\n", fp);
	fputs("She was studying with writing something.\n", fp);
	fputs("I can't do anything but just looking her.\n", fp);
	
	//fputs("01234", fp);



	printFileSize(fp);

	fclose(fp);

	return 0;
}


void printFileSize(FILE *fp)
{
	long fpos=0;
	long end=0;

	fpos = ftell(fp);

	fseek(fp, 0, SEEK_END);
	end = ftell(fp);

	//move fp pointer to the original location.
	fseek(fp, fpos, SEEK_SET);

	printf("%li\n", end);

	return;
}
