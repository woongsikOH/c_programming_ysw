#include <stdio.h>

int main()
{
	int *ptr1=0x0010;
	double *ptr2=0x0010;

	printf("%p %p \n", ptr1+1, ptr1+2);	//being increased 4, 8
	printf("%p %p \n", ptr2+1, ptr2+2);	//being increased 8, 16

	printf("%p %p \n", ptr1, ptr2);

	ptr1++;	//being increased 4
	ptr2++;	//being increased 8
	printf("%p %p \n", ptr1, ptr2);
	
	return 0;
}
