#include <stdio.h>

int main()
{
	int arr[3]={0, 1, 2};
	printf("Name of array : %p \n", arr);
	printf("arr[0] : %p \n", &arr[0]);
	printf("arr[1] : %p \n", &arr[1]);
	printf("arr[2] : %p \n", &arr[2]);

	//arr = &arr[2];	//error. because arr is constant pointer type

	return 0;
}
