#include <stdio.h>

int main()
{
	char str1[]="Variable string";
	char *str2="Constant string";
	printf("%s %s \n", str1, str2);

	printf("size of strings\nstr1 : %lu, str2 : %lu \n", sizeof(str1), sizeof(str2));

	str2="Changable string";
	printf("%s %s \n", str1, str2);

	str1[0]='X';
	//str2[0]='X';	//core dumped
	printf("%s %s \n", str1, str2);

	return 0;
}
