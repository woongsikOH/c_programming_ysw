#include <stdio.h>

int main()
{
	int cnt=1;
	int num=2;	//number for test whethere it is prime or not.
	int prime=1;

	//knowing that 2 is prime, print it before start.
	printf("2");

	//loop until find 10 prime numbers
	while(cnt<9)
	{
		//start from num3, knowing 2 is a prime.
		num++;

		for(int i=2; i<num; i++)
		{
			//If the number is not a prime, break. 
			if(num%i==0)
			{
				prime=0;
				break;
			}
		}

		if(prime)
		{

			//If it is not broken during for loop, It is prime
			printf(", %d", num);
			cnt++;
		}
		else
			prime=1;
	}

	//when found 10prime(including 2)
	printf(" FIN\n");

	return 0;
}
