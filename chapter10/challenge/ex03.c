#include <stdio.h>

int findGCD(int, int);

int main()
{
	int num1=0, num2=0;
	int GCD=0;

	printf("Enter two 'plus demical integers' : ");
	scanf("%d %d", &num1, &num2);
	
	printf("input integers' GCD : %d \n", findGCD(num1, num2));

	return 0;
}

int findGCD(int num1, int num2)
{
	int max=0;
	int GCD=0;

	max = (num1>num2) ? num1:num2;

	for(int i=1; i<=max; i++)
	{
		if(num1%i==0 && num2%i==0)
			GCD=i;
	}

	return GCD;
}
