#include <stdio.h>

int power(int, int);

int main()
{
	int num1=0, num2=0;

	printf("Enter demical integer : ");
	scanf("%d %d", &num1, &num2);

	if(num2 >= 0)
		printf("%d power %d : %d \n", num1, num2, power(num1, num2));

	//else if(num2 == 0)
	//	printf("%d power %d : 1\n", num1, num2);

	else 
		printf("%d power %d : 1/%d \n", num1, num2, power(num1, num2));

	return 0;
}

int power(int num1, int num2)
{
	if(num2 == 0)
		return 1;
	else
		return num1*power(num1, num2-1);
}
