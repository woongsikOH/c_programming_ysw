#include <stdio.h>

void secToHMS(int);

int main()
{
	int sec=0;

	printf("Enter seconds : ");
	scanf("%d", &sec);

	secToHMS(sec);

	return 0;
}

void secToHMS(int sec)
{
	if(sec < 60)
		printf("s:%d \n", sec);
	else if(sec < 3600)
		printf("m:%d s:%d \n", sec/60, sec%60);
	else
		printf("h:%d m:%d s:%d \n", sec/3600, (sec/60)%60, sec%60);
}
