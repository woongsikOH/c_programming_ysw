#include <stdio.h>

void printMulTable();
void printMulSingle(int);

int main()
{
	printMulTable();

	return 0;
}

void printMulTable()
{
	int num1=0, num2=0;

	printf("Enter two demical integers : ");
	scanf("%d %d", &num1, &num2);

	if(num1 == num2)
		printMulSingle(num1);
	
	else if(num1 > num2)
	{
		for(int i=num2; i<=num1; i++)
			printMulSingle(i);
	}

	else
	{
		for(int i=num1; i<=num2; i++)
			printMulSingle(i);
	}
}

void printMulSingle(int num)
{
	printf("%d's multiplication table.\n", num);
	
	for(int i=1; i<10; i++)
		printf("%d X %d = %d \n", num, i, num*i);

	printf("\n");
}
