#include <stdio.h>

int main()
{
	const int PRICE_SHIRIMPSNACK = 700;
	const int PRICE_CREAMBREAD = 500;
	const int PRICE_COKE = 400;

	int money=0;
	int total=0;
	int cnt=0;
	int count=0;

	// input money
	printf("Enter your present money : ");
	scanf("%d", &money);

	// If it is less than 400, print error
	if(money < PRICE_COKE)
	{
		printf("Not enough money to buy somthing.\n");
		return 0;
	}

	// else, print amount of money
	else
	{
		printf("Your money : %d\n", money);
		printf("-------------------------------------------------\n");
	}

	// add money by category.
	for(int i=0; i<=money/PRICE_SHIRIMPSNACK; i++)
	{
		total=(PRICE_SHIRIMPSNACK*i);

		if(total==money)
		{
			count++;
			printf("Shirimp Snack : %d \n", i);
			break;
		}

		cnt=(money-total)/PRICE_CREAMBREAD;
		for(int j=0; j<=cnt; j++)
		{
			total=(PRICE_SHIRIMPSNACK*i)+(PRICE_CREAMBREAD*j);

			if(total==money)
			{
				count++;
				printf("Shirimp Snack : %d, Cream Bread : %d \n", i, j);
				break;
			}
			
			cnt=(money-total)/PRICE_COKE;
			for(int k=1; k<=cnt; k++)
			{
				total=(PRICE_SHIRIMPSNACK*i)+(PRICE_CREAMBREAD*j)+(PRICE_COKE*k);
				
				if(total==money)	
				{
					count++;
					printf("Shirimp Snack : %d, Cream Bread : %d, Coke : %d \n", i, j, k);
					break;
				}
			}
		}
	}

	switch(count)
	{
		case 0:
			printf("There is no way to buy goods at price.\n");
			break;
			
		case1:
			printf("There is only 1way to buy goods at price.\n");
			break;
			
		default:
			printf("There are %d ways to buy goods at price.\n", count);
			printf("-------------------------------------------------\n\n");
	}

	return 0;
}
		
