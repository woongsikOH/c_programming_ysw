#include <stdio.h>

int powerOfTwo(int);

int main()
{
	int num=0, cnt=0;

	printf("Enter a positive demical integer : ");
	scanf("%d", &num);

	if(num < 1)
		printf("No possible k\n");

	else if(num == 1)
		printf("Maximum k=0, 2^k=1\n");

	else
	{
		while(1)
		{
			num/=2;
			cnt++;

			if(num == 1)
				break;
		}

		printf("Maximum k=%d, 2^k=%d \n", cnt, powerOfTwo(cnt));
	}

	return 0;
}

int powerOfTwo(int num)
{
	if(num == 0)
		return 1;
	else
		return 2*powerOfTwo(num-1);
}
