#include <stdio.h>

int findGCD(int, int);
int uclid_GCD(int, int);

int main()
{
	int num1=0, num2=0;
	int GCD=0;

	printf("Enter two 'plus demical integers' : ");
	scanf("%d %d", &num1, &num2);
	
	//printf("input integers' GCD : %d \n", findGCD(num1, num2));
	printf("input integers' GCD : %d \n", uclid_GCD(num1, num2)); 

	return 0;
}

int findGCD(int num1, int num2)
{
	int max=0;
	int GCD=0;

	max = (num1>num2) ? num1:num2;

	for(int i=1; i<=max; i++)
	{
		if(num1%i==0 && num2%i==0)
			GCD=i;
	}

	return GCD;
}

int uclid_GCD(int num1, int num2)
{
	int temp=0;

	if(num1 < num2)
	{
		temp = num1;
		num2 = num1;
		num1 = temp;
	}

	while(1)
	{
		temp=num1%num2;

		if(temp==0)
		{
			return num2;
			break;
		}

		num1=num2;
		num2=temp;
	}

	return 0;
}
