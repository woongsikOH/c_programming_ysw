#include <stdio.h>

int main()
{
	int arr[2][2][2] = {0};

	printf("arr's ardress : %p \n", &arr);
	printf("[0][0][0] : %p \n", &arr[0][0][0]);
	printf("[1][0][0] : %p \n", &arr[1][0][0]);
	printf("[0][1][0] : %p \n", &arr[0][1][0]);
	printf("[0][0][1] : %p \n", &arr[0][0][1]);
	printf("\n");

	printf("[0][1] : %p \n", &arr[0][1]);
	printf("[1][0] : %p \n", &arr[1][0]);
	printf("[1] : %p \n", &arr[1]);
	printf("\n");

	return 0;
}
