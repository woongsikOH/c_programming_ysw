#include <stdio.h>

int main()
{
	char word[50]={0};
	int length=0;
	int idx=0;
	
	printf("Enter a word : ");
	scanf("%s", word);

	while(word[idx] != 0)
	{
		length++;
		idx++;
	}

	printf("The length of the word : %d \n", length);

	return 0;
}
