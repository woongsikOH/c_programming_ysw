#include <stdio.h>

int main()
{
	char word[50]={0};
	int idx=0;
	char max = 0;
	
	printf("Enter a word : ");
	scanf("%s", word);

	while(word[idx] != 0)
	{
		if(word[idx] > max)
			max = word[idx];

		idx++;
	}

	printf("The biggest ASCII character : %c \n", max);

	return 0;
}
