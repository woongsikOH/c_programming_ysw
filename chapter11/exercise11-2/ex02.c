#include <stdio.h>

int main()
{
	char word[50]={0};
	char temp = 0;

	int length=0;
	int idx=0;
	
	printf("Enter a word : ");
	scanf("%s", word);

	while(word[idx] != 0)
	{
		idx++;
	}

	length = idx;
	idx=0;

	while(idx != length/2)
	{
		temp = word[length-1-idx];
		word[length-1-idx] = word[idx];
		word[idx] = temp;

		//printf("\nmove%d \nidx=%d \nword[idx]=%c \nword[length-1-idx]=%c \n", idx+1, idx, word[idx], word[length-1-idx]);
		
		idx++;
	}

	printf("Input word : %s \n", word);

	return 0;
}
