#include <stdio.h>

int main()
{
	int arr[5]={0};
	int max=0;
	int min=0;
	int sum=0;

	printf("Enter 5 demical integers : ");
	
	for(int i=0; i<5; i++)
		scanf("%d", &arr[i]);
	

	max=arr[0];
	min=arr[0];
	sum=arr[0];
	for(int i=1; i<5; i++)
	{
		if(arr[i] > max)
			max = arr[i];

		if(arr[i] < min)
			min = arr[i];
		
		sum+=arr[i];
	}

	printf("maximum int of arr : %d \n", max);
	printf("minimum int of arr : %d \n", min);
	printf("summation int of arr : %d \n", sum);

	
	/*
	//print all integers in array
	for(int i=0; i<5; i++)
		printf("arr[%d]=%d \n", i, arr[i]);
	printf("\n");
	*/
	

	return 0;
}
