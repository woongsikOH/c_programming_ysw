#include <stdio.h>

int main()
{
	char str[50];
	int idx=0;
	
	printf("Enter string : ");
	scanf("%s", str);	//not &str, but str
	printf("print input string : %s \n", str);

	printf("print string char by char : ");

	while(str[idx] != '\0')
	{
		printf("%c", str[idx]);
		idx++;
	}
	printf("\n");

	return 0;
}
