#include <stdio.h>

int main()
{
	char str[]="Good morning!";

	printf("size of array str : %lu \n", sizeof(str));
	printf("print 'null' in char type : %c \n", str[13]);
	printf("print 'null' in int type : %d \n", str[13]);

	str[12]='?';
	printf("print string : %s \n", str);

	return 0;
}
