#include <stdio.h>

int main()
{
	int villa[4][2]={0};
	int popu=0;

	//Input the number of residence.
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<2; j++)
		{
			printf("Enter the number of %dfloor, %droom residence : ", i, j);
			scanf("%d", &villa[i][j]);
		}
	}
	printf("\n");

	//print the number of residence on the same floor.
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<2; j++)
			popu+=villa[i][j];

		printf("The number of %d floor's residence : %d \n", i, popu);
		popu=0;
	}

	return 0;
}
