#include <stdio.h>

int main()
{
	//Initializing example 1
	int arr1[3][3]={
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9}
	};
	
	//Initializing example 2
	int arr2[3][3]={
		{1},
		{4, 5},
		{7, 8, 9}
	};


	//Initializing example 3
	int arr3[3][3]={1, 2, 3, 4, 5, 6, 7};

	//print the result
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
			printf("%d ", arr1[i][j]);
		printf("\n");
	}
	printf("\n");
	
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
			printf("%d ", arr2[i][j]);
		printf("\n");
	}
	printf("\n");
	
	for(int i=0; i<3; i++)
	{
		for(int j=0; j<3; j++)
			printf("%d ", arr3[i][j]);
		printf("\n");
	}
	printf("\n");
	

	return 0;
}
