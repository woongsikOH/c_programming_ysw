#include <stdio.h>

int main()
{
	int arr1[3][4]={0};
	int arr2[7][9]={0};

	printf("size of 3rows 4columns array : %lu \n", sizeof(arr1));
	printf("size of 7rows 9columns array : %lu \n", sizeof(arr2));

	return 0;
}
