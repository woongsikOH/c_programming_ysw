#include <stdio.h>

int main()
{
	int multitabel[3][9]={0};

	for(int i=0; i<3; i++)
	{
		for(int j=0; j<9; j++)
			multitabel[i][j] = (i+2)*(j+1);
	}

				
	for(int i=0; i<3; i++)
	{
		printf("Multiply result of %d \n", i+2);

		for(int j=0; j<9; j++)
			printf("%d ", multitabel[i][j]);
		printf("\n\n");
	}

	return 0;
}
