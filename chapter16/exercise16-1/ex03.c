#include <stdio.h>

int main()
{
	int scoretable[5][5]={0};
	int personaltotal=0;

	int kor=0, eng=0, math=0, his=0; 

	for(int i=0; i<4; i++)
	{
		printf("Enter 5 subjects' score : ");
		
		for(int j=0; j<4; j++)
		{
			scanf("%d", &scoretable[i][j]);
			personaltotal+=scoretable[i][j];

			switch(j)
			{
				case 0:
					kor += scoretable[i][j];
					break;
					
				case 1:
					eng += scoretable[i][j];
					break;
					
				case 2:
					math += scoretable[i][j];
					break;
					
				case 3:
					his += scoretable[i][j];
					
			}
		}

		scoretable[i][4] = personaltotal;
		personaltotal=0;
	}

	scoretable[4][0] = kor;
	scoretable[4][1] = eng;
	scoretable[4][2] = math;
	scoretable[4][3] = his;
	scoretable[4][4] = kor + eng + math + his;

	printf("         kor  eng  math  his total\n");
	
	printf("철희 ");
	for(int j=0; j<5; j++)
		printf("%5d ", scoretable[0][j]);
	printf("\n");
		
	printf("철수 ");
	for(int j=0; j<5; j++)
		printf("%5d ", scoretable[1][j]);
	printf("\n");
		
	printf("영희 ");
	for(int j=0; j<5; j++)
		printf("%5d ", scoretable[2][j]);
	printf("\n");
		
	printf("영수 ");
	for(int j=0; j<5; j++)
		printf("%5d ", scoretable[3][j]);
	printf("\n");

	printf("total");
	for(int j=0; j<5; j++)
		printf("%5d ", scoretable[4][j]);
	printf("\n");
		
	return 0;
}	
