//입력 받는것을 함수로 대체할 때, 그 제한 길이가 줄어드는 이유 찾고 수정해보기

#include <stdio.h>
#include <string.h>

void removeWn(char *);
void clearStdin();

void putEmployeeInfo(char *, char*, int*);
void printEmployeeInfo(const char *, const char *, int);

struct employee
{
	char name[20];
	char ssid[20];
	int wage;
};

int main()
{
	struct employee person01, person02;

	putEmployeeInfo(person01.name, person01.ssid, &person01.wage);//, strlen(person01.name), strlen(person01.ssid));
	putEmployeeInfo(person02.name, person02.ssid, &person02.wage);//, strlen(person02.name), strlen(person02.ssid));

	printEmployeeInfo(person01.name, person01.ssid, person01.wage);
	printEmployeeInfo(person02.name, person02.ssid, person02.wage);

	return 0;
}

void removeWn(char *str)
{
	int leng = strlen(str);

	if(str[leng-1] == '\n')
		str[leng-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar()!='\n');
}

void putEmployeeInfo(char *name, char *ssid, int *wage)//, int leng_name, int leng_ssid)
{
	putchar('\n');
	puts("Typing new employee's info.");
	fputs("Enter a name : ", stdout); fgets(name, sizeof(name), stdin);
	removeWn(name);

	fputs("Enter a SSID : ", stdout); fgets(ssid, sizeof(name), stdin);
	removeWn(ssid);

	fputs("Enter a wage : ", stdout); scanf("%d", wage);
	clearStdin();	//when takes integer, there's enter and it lasts in the stdin.
}

void printEmployeeInfo(const char* name, const char* ssid, const int wage)
{
	putchar('\n');
	printf("Name : %s \n", name);
	printf("SSID : %s \n", ssid);
	printf("Wage : %d \n", wage);
}
