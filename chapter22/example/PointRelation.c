#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
	struct point *ptr;
};

int main()
{
	struct point p1 = {1,1};
	struct point p2 = {2,2};
	struct point p3 = {3,3};

	p1.ptr = &p2;
	p2.ptr = &p3;
	p3.ptr = &p1;

	printf("Point p1[%d,%d] is relate to p2[%d,%d] \n", p1.xpos, p1.ypos, p1.ptr->xpos, p1.ptr->ypos);
	printf("Point p2[%d,%d] is relate to p3[%d,%d] \n", p2.xpos, p2.ypos, p2.ptr->xpos, p2.ptr->ypos);
	printf("Point p3[%d,%d] is relate to p1[%d,%d] \n", p3.xpos, p3.ypos, p3.ptr->xpos, p3.ptr->ypos);

	return 0;
}
