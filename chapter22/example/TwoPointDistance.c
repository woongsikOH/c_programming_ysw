#include <stdio.h>
#include <math.h>

struct point
{
	int xpos;
	int ypos;
};

int main()
{
	struct point pos1, pos2;
	double distance=0;

	fputs("Enter point 1 position : ", stdout);
	scanf("%d %d", &pos1.xpos, &pos1.ypos);

	fputs("Enter point 2 position : ", stdout);
	scanf("%d %d", &pos2.xpos, &pos2.ypos);

	distance = sqrt(pow((double)pos1.xpos - pos2.xpos, 2) + pow((double)pos1.ypos - pos2.ypos, 2));

	printf("Distance of two positions : %g \n", distance);


	return 0;
}
