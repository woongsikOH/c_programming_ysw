#include <stdio.h>
#include <string.h>

void removeWn(char *);
void clearStdin();

struct person
{
	char name[20];
	char phoneNum[20];
	int age;
};

int main()
{
	struct person man1, man2;
	strcpy(man1.name, "Lenard\0");
	strcpy(man1.phoneNum, "010-123-4567\0");
	man1.age = 30;

	/*
	 * scanf method
	 *
	printf("Enter a name : "); scanf("%s", man2.name);
	printf("Enter a phone number : "); scanf("%s", man2.phoneNum);
	printf("Enter an age : "); scanf("%d", &man2.age);
	*/
	
	fputs("Enter a name : ", stdout);
	fgets(man2.name, sizeof(man2.name), stdin);
	removeWn(man2.name);

	fputs("Enter a phone number : ", stdout);
	fgets(man2.phoneNum, sizeof(man2.phoneNum), stdin);
	removeWn(man2.phoneNum);
	
	fputs("Enter an age : ", stdout); scanf("%d", &man2.age);

	//print all infos.
	//person1
	putchar('\n');
	printf("Name : %s \n", man1.name);
	printf("Phone number : %s \n", man1.phoneNum);
	printf("Age : %d \n", man1.age);

	//person2
	putchar('\n');
	printf("Name : %s \n", man2.name);
	printf("Phone number : %s \n", man2.phoneNum);
	printf("Age : %d \n", man2.age);

	return 0;
}

void removeWn(char *str)
{
	int leng = strlen(str);

	if(str[leng-1] == '\n')
		str[leng-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar()!='\n');
}
