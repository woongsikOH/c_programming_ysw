#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
};

struct person
{
	char name[20];
	char phoneNum[20];
	int age;
};

int main()
{
	struct point point1={10, 20};
	struct person person1={"Sheldon Cooper", "034-123-12", 25};

	printf("%d %d \n", point1.xpos, point1.ypos);
	printf("%s %s %d \n", person1.name, person1.phoneNum, person1.age);

	return 0;
}
