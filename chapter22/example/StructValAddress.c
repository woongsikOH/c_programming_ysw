#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
};

struct person
{
	char name[20];
	char phoneNum[20];
	int age;
};

int main()
{
	struct point point01={10, 20};
	struct person person01={"Lenard", "010-123-1234", 34};

	printf("%p %p \n", &point01, &person01);
	printf("%p %p \n", &point01.xpos, person01.name);


	return 0;
}
	
