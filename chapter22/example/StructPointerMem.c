#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
};

struct circle
{
	double radius;
	struct point *center;
};

int main()
{
	struct point center={0,0};
	double radius = 5.5;

	struct circle circle_01 = { radius, &center };

	printf("The radius of the circle_01 : %f \n", circle_01.radius);
	printf("The center of the circle_01 : [%d, %d] \n", circle_01.center->xpos, circle_01.center->ypos);

	return 0;
}
