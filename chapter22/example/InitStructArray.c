#include <stdio.h>

struct person
{
	char name[20];
	char phoneNum[20];
	int age;
};

int main()
{
	struct person arr[3]={
		{"penny", "082-121-143", 21},
		{"Lenard", "012-1233-14", 22},
		{"Sheldon", "919-10-11", 20}
	};

	for(int i=0; i<3; i++)
		printf("%s %s %d \n", arr[i].name, arr[i].phoneNum, arr[i].age);


	return 0;
}
