#include <stdio.h>

struct point
{
	int xpos;
	int ypos;
};

int main()
{
	struct point pointArr[3];

	for(int i=0; i<3; i++)
	{
		fputs("Enter x y cordinates : ", stdout);
		scanf("%d %d", &pointArr[i].xpos, &pointArr[i].ypos);
	}

	for(int i=0; i<3; i++)
		printf("pointArr[i]'s x,y cordinates : %d , %d \n", pointArr[i].xpos, pointArr[i].ypos);

	return 0;
}
