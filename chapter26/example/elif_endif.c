#include <stdio.h>
#define HIT_NUM	7

int main()
{
#if HIT_NUM==5
	puts("HIT_NUM MACRO is 5.");
#elif HIT_NUM==6
	puts("HIT_NUM MACRO is 6.");
#elif HIT_NUM==7
	puts("HIT_NUM MACRO is 7.");
#else
	puts("HIT_NUM MACRO isn't 5 or 6 or 7.");
#endif

	return 0;
}
