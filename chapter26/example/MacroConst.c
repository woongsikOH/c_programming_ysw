#include <stdio.h>

#define NAME	"홍길동"
#define AGE	24
#define PRINT_ADDR	puts("주소 : 경기도 용인시")

int main()
{
	printf("Name : %s \n", NAME);
	printf("Age  : %d \n", AGE);
	PRINT_ADDR;
	
	return 0;
}
