#include <stdio.h>

#define HIT_NUM	5

int main()
{
#if HIT_NUM==5
	puts("HIT_NUM MACRO is defined and its value is 5.");
#else
	puts("HIT_NUM MACRO isn't defined.");
#endif

	return 0;
}
