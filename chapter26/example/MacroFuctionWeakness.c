#include <stdio.h>

#define DIFF(X, Y)	((X)>(Y)) ? ((X)-(Y)) : ((Y)-(X))

int main()
{
	printf("difference of two variables : %d \n", DIFF(5, 7));
	printf("difference of two variables : %g \n", DIFF(1.8, -1.4));

	return 0;
}
