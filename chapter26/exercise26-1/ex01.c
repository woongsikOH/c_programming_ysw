#include <stdio.h>

#define ADD3NUM(X,Y,Z)	((X)+(Y)+(Z))
#define MUL3NUM(X,Y,Z)	((X)*(Y)*(Z))

int main()
{
	int num1=0, num2=0, num3=0;

	fputs("Enter three integers : ", stdout);
	scanf("%d %d %d", &num1, &num2, &num3);

	printf("The summation : %d \n", ADD3NUM(num1, num2, num3));
	printf("The multipication : %d \n", MUL3NUM(num1, num2, num3));

	return 0;
}
	
