#include <stdio.h>

#define MAX(X,Y)	((X)>(Y)) ? (X) : (Y)

int main()
{
	printf("between 3 and 3.786, the bigger number is %g \n", MAX(3, 3.786));
	//fputs("Enter two numbers : ");
	return 0;
}
