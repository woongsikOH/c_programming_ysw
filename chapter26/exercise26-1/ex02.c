#include <stdio.h>

#define PI	3.141592
#define CIRCLE_AREA(X)	((X)*(X)*PI)

int main()
{
	double rad=0.0;

	fputs("Enter a radius of circle : ", stdout);
	scanf("%lf", &rad);
	printf("The area of the circle : %g \n", CIRCLE_AREA(rad));

	return 0;
}
