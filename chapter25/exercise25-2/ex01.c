#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * getString();
void printWordsBackward(const char *, const int);
void removeWn(char *);
void clearStdin();

int main()
{
	char *str;

	while(1)
	{
		str=getString();
		printWordsBackward(str, strlen(str));
		free(str);
	}
	

	return 0;
}

void printWordsBackward(const char *str, const int length)
{
	int idx = length;

	for(int i=length-1; i>=0; i--)
	{
		if(str[i] == ' ')
		{
			for(int j=i+1; j<idx; j++)
				putchar(str[j]);
			putchar(' ');

			idx = i;
		}
	}
	putchar('\n');
}



char * getString()
{
	int length=0;
	char *str;

	fputs("Enter the length of sentence you'll type : ", stdout);
	scanf("%d", &length);
	clearStdin();

	str = (char *)calloc(length+1, sizeof(char));

	fputs("Enter the string : ", stdout);
	str[0]=' ';
	fgets(str+1, length, stdin);
	removeWn(str);

	return str;
}


void removeWn(char *str)
{
	int length = strlen(str);

	if(length > 0 && str[length-1] == '\n')
		str[length-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar() != '\n');
}
