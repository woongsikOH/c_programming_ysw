#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i=0;
	int *ptr = (int *)calloc(5, sizeof(int));

	while(1)
	{
		fputs("Enter a integer (-1 to quit) : ", stdout);
		scanf("%d", &ptr[i]);

		if(ptr[i] == -1)
		{
			for(int j=0; j<i; j++)
				printf("%d ", ptr[j]);
			putchar('\n');

			break;
		}

		//realloc
		if((i-1)%3 == 0)
			ptr = (int *)realloc(ptr, sizeof(int)*(i+4));


		i++; 
	}
		
	return 0;
}
