#include <stdio.h>

char *ReadUserName(void)
{
	char name[30];
	printf("What's your name? : ");
	gets(name);

	return name;
}

void saveUserName(char *str)
{
	fputs("What's your name? : ", stdout);
	//gets(str);
	scanf("%s", str);

	return;
}

int main()
{
	char *name1;
	char name2[20];

	name1=ReadUserName();
	printf("name1 : %s \n", name1);

	saveUserName(name2);
	printf("name2 : %s \n", name2);

	return 0;
}
