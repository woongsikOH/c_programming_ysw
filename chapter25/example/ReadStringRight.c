#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeWn(char *str)
{
	int leng = strlen(str);

	if(leng > 0 && str[leng-1] == '\n')
		str[leng-1] = 0;
}

char * saveUsername()
{
	char *name = (char *)malloc(sizeof(char)*30);
	fputs("Enter your name : ", stdout);
	fgets(name, 30, stdin);

	removeWn(name);

	return name;
}


int main()
{
	char *name1;
	char *name2;

	name1 = saveUsername();
	printf("Name1 : %s \n", name1);

	name2 = saveUsername();
	printf("Name2 : %s \n", name2);

	printf("Name1 : %s \n", name1);
	printf("Name2 : %s \n", name2);

	free(name1);
	free(name2);

	return 0;
}
