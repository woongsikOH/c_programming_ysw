#include <stdio.h>

int main()
{

	int num1, num2, num3;

	printf("This program gets 3 decimal integer and print the output\n");
	printf("(OUTPUT : num1 x num2 + num3 = result)\n\n");

	printf("Input 3 decimal integer : ");
	scanf("%d %d %d", &num1, &num2, &num3);

	printf("%d * %d + %d = %d \n", num1, num2, num3, num1 * num2 + num3);

	return 0;
}	
