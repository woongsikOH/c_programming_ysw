#include <stdio.h>

int main()
{
	int num1, num2;

	printf("Input two decimal integer : ");
	scanf("%d %d",&num1, &num2);

	printf("\n");
	printf("Summation : %d \n", num1+num2);
	printf("Subtraction : %d \n", num1-num2);
	printf("Multiplication : %d \n", num1*num2);

	return 0;
}
