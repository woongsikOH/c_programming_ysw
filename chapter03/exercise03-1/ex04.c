#include <stdio.h>

int main()
{

	int num1, num2;

	printf("This program gets 2 decimal integer and print the output\n");
	printf("(OUTPUT : num1/num2's quotient, num1/num2's remain)\n\n");

	printf("Input 2 decimal integer : ");
	scanf("%d %d", &num1, &num2);

	printf("Quatient : %d, Remain : %d \n", num1/num2, num1%num2);

	return 0;
}	
