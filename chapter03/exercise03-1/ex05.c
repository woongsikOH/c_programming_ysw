#include <stdio.h>

int main()
{

	int num1, num2, num3;

	printf("This program gets 3 decimal integer and print the output\n");
	printf("( OUTPUT : (num1-num2)*(num2+num3)*(num3%%num1) )\n\n");

	printf("Input 3 decimal integer : ");
	scanf("%d %d %d", &num1, &num2, &num3);

	printf("%d \n", (num1-num2)*(num2+num3)*(num3%num1));

	return 0;
}	
