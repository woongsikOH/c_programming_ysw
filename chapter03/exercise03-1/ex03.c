#include <stdio.h>

int main()
{

	int num;

	printf("This program gets 1 decimal integer and print the output\n");
	printf("(OUTPUT : (num)^2)\n\n");

	printf("Input decimal integer : ");
	scanf("%d", &num);

	printf("%d \n", num * num);

	return 0;
}	
