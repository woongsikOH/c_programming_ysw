#include <stdio.h>

int main()
{
	int result;
	int num1, num2, num3;

	printf("Input 3 decimal integer : ");
	//scanf로 변수 여러개를 동시에 입력 받을 때는 ','로 구분하면 오류남
	scanf("%d %d %d", &num1, &num2, &num3);

	result = num1 + num2 + num3;
	printf("%d + %d + %d = %d \n", num1, num2, num3, result);

	return 0;
}
