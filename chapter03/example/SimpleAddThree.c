#include <stdio.h>

int main()
{
	int result;
	int num1, num2;

	printf("decimal integer : ");
	scanf("%d", &num1);

	printf("hexadecimal integer : ");
	scanf("%x", &num2);

	result = num1 + num2;

	printf("decimal num summation\n");
	printf("%d + %x = %x \n", num1, num2, result);

	printf("hexadecimal num summation\n");
	printf("%d + %x = %d \n", num1, num2, result);

	return 0;
}
