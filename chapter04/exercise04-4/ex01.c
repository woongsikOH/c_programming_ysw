#include <stdio.h>

int main()
{
	int num;

	printf("This program gets 1 decimal integer and print the negative of that\n");
	printf("Input 1 decimal integer : ");
	scanf("%d",&num);

	num = ~num + 1;
	printf("Result : %d \n", num);

	return 0;
}
