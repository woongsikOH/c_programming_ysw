#include <stdio.h>

int main()
{
	int num1 = 15; // 00000000 00000000 00000000 00001111
	int num2 = 20; // 00000000 00000000 00000000 00010100
	int num3 = num1 & num2;

	printf("num1 : %d, num2 : %d, num1&num2 : %d \n", num1, num2, num3);
	// printf("num1 : %d, num2 : %d, num1&num2 : %d \n", num1, num2, num1&num2);

	return 0;
}
