#include <stdio.h>

int main()
{
	int num = -16; // 11111111 11111111 11111111 11110000
	printf("2 right bit shift : %d \n", num >> 2);
	printf("3 right bit shift : %d \n", num >> 3);

	return 0;
}
