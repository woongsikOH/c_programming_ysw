#include <stdio.h>
int AbsoCompare(int, int);
int GetAbsoValue(int);

int main()
{
	int num1=0, num2=0;

	printf("Enter two integers : ");
	scanf("%d %d", &num1, &num2);

	printf("The number that has bigger Absolute value : %d \n", AbsoCompare(num1, num2));
	return 0;
}

int AbsoCompare(int num1, int num2)
{
	return (GetAbsoValue(num1)>GetAbsoValue(num2)) ? num1:num2;
}

int GetAbsoValue(int num)
{
	return (num>=0) ? num:num*-1;
}
