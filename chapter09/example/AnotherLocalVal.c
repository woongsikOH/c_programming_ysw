#include <stdio.h>

int main()
{
	for(int cnt=0; cnt<3; cnt++)
	{
		int num=0;
		num++;

		printf("%d번째 반복, 지역변수 num은 %d. \n", cnt+1, num);
	}

	if(1)
	{
		int num=7;
		num++;
		printf("if문 내에 존재하는 지역변수 num은 %d. \n", num);
	}

	return 0;
}
