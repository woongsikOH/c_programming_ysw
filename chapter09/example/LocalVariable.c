#include <stdio.h>

int SimpleFuncOne()
{
	int num=10;
	num++;
	printf("SimpleFuncOne num : %d \n", num);
	
	return 0;	// SimpleFuncOne()의 num변수가 유효한 마지막 문장
}

int SimpleFuncTwo()
{
	int num1=20;
	int num2=30;
	num1++, num2--;
	printf("num1 & num2 : %d %d \n", num1, num2);
	
	return 0;	// SimpleFuncTwo()의 num1, num2 변수가 유효한 마지막 문장
}

int main()
{
	int num=17;
	SimpleFuncOne();
	SimpleFuncTwo();
	printf("main num : %d \n", num);
	
	return 0;	// main의 num이 유효한 마지막 문장
}
