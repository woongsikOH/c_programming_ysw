#include <stdio.h>

int Add(int num1, int num2){
	return num1+num2;
}

int main()
{
	int result;

	result = Add(3, 4);
	printf("Addition result : %d \n", result);

	result = Add(5, 8);
	printf("Addition result : %d \n", result);
	
	printf("Addition result : %d \n", Add(3, 4));

	return 0;
}
