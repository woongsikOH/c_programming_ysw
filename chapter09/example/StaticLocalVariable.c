#include <stdio.h>

void SimpleFunc()
{
	static int num1=0; //초기화 없어도 자동 0 초기화
	int num2=0; //초기화 없으면 null값

	num1++, num2++;

	printf("static : %d, local %d \n", num1, num2);
}

int main()
{
	for(int i=0; i<3; i++)
		SimpleFunc();

	//printf("num : %d \n", num1);	//compile error. undeclared variable

	return 0;
}
