#include <stdio.h>

int Add(int num1, int num2){

	return num1+num2;
}

void ShowAddResult(int num){

	printf("Addition result : %d \n", num);
}

int ReadNum(){

	int num;
	scanf("%d", &num);
	return num;
}

void HowToUseThisProg(){

	printf("Enter two integers, Then the program prints the addition result.\n");
	printf("Enter two integers : ");
}

int main()
{

	int result=0, num1=0, num2=0;

	HowToUseThisProg();
	
	num1=ReadNum();
	num2=ReadNum();
	
	result = Add(num1, num2);

	ShowAddResult(result);

	return 0;
}
