#include <stdio.h>

void fibo(int);

int main()
{
	int num=0;

	printf("Enter number amount of fibonacci you want : ");
	scanf("%d", &num);

	fibo(num);

	return 0;
}

void fibo(int num)
{

	int num1=0, num2=1, temp=0;	

	if(num <= 0)
		return;

	switch(num)
	{
		case 1:
			printf("0 Fin\n");
			break;
			
		case 2:
			printf("0, 1 Fin\n");
			break;

		default:

			printf("0, 1");
			for(int i=2; i<num; i++){

				temp = num1 + num2;
				num1 = num2;
				num2 = temp;

				printf(", %d", temp);
			}
			printf(" Fin\n");
	}
}
