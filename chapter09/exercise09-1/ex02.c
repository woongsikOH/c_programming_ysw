#include <stdio.h>

double celToFah(double);
double fahToCel(double);

int main()
{
	char mode=0;
	double tem=0.0;

	printf("Select mode\n[1.Celsius to Fahrenheit 2.Fahrenheit to Celsius]\n");
	scanf("%c", &mode);

	printf("Enter the present temperature : ");
	scanf("%lf", &tem);

	switch(mode)
	{
	case '1':
		printf("%f'C = %f'F\n", tem, celToFah(tem));
		break;
		
	case '2':
		printf("%f'F = %f'C\n", tem, fahToCel(tem));
	}

	return 0;
}

double celToFah(double tem)
{
	return 1.8*tem+32;
}

double fahToCel(double tem)
{
	return (tem-32)*5/9;
}
