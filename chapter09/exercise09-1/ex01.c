#include <stdio.h>

int maxInt(int, int, int);
int minInt(int, int, int);

int main()
{
	int num1=0, num2=0, num3=0;
	printf("Enter 3 demical integers : ");
	scanf("%d %d %d", &num1, &num2, &num3);

	printf("The maximum integer is : %d \n", maxInt(num1, num2,num3));
	printf("The minimum integer is : %d \n", minInt(num1, num2,num3));
	printf("\n");

	return 0;
}

int maxInt(int num1, int num2, int num3)
{
	int max;
	max = (num1 > num2) ? num1 : num2;
	max = (max > num3) ? max : num3;

	return max;
}


int minInt(int num1, int num2, int num3)
{
	int min;
	min = (num1 < num2) ? num1 : num2;
	min = (min < num3) ? min : num3;
}
