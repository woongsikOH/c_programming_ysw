#include <stdio.h>

void printSnailArr(int);
void printArr(int, int (*)[]);
void printPartArr(int, int (*)[]);

int main()
{
	int num=0;

	printf("Enter a demical integer : ");
	scanf("%d", &num);

	printSnailArr(num);
	
	//for test
	printf("\nfor the test\n");
	int test[3][3] = {1,2,3,4,5,6,7,8,9};
	printArr(2, test);
	printArr(3, test);

	return 0;
}

void printSnailArr(int num)
{
	int i=0, j=0, l=0;
	int cnt=1;	/*to fill the array number by number
			  saving for next write
			  */
	int orignal_num = num;

	int arr[num][num];

	//process0
	for(l=0; l<num; l++)	//filling the first row
	{
		arr[i][j] = cnt;
		cnt++;	//save the progress
		j++;
	}
	j--;	//(restroing j++) 

	//if array is 1x1, print and return;
	if(num == 1)
	{
		printArr(1, arr);
		return;
	}


	while(num>0)
	{

	/////////////////////////////////////
	//row and column increasing pattern//
	/////////////////////////////////////
	

	//process1
	num--;	//number filling is decreaed
	i++;	//for next filling
	for(l=0; l<num; l++)
	{
		arr[i][j] = cnt;
		cnt++;
		i++;
	}
	i--;	//(restroing i++)


	/////////////////////////////////////
	//row and cloumn decreasing pattern//
	/////////////////////////////////////
	
	//process2
	j--;	//for next filling
	for(l=0; l<num; l++)
	{
		arr[i][j] = cnt;
		cnt++;
		j--;
	}
	j++;	//restoring j--;

	//process3
	num--;	//number filling is decreased
	i--;	//for nextfilling
	for(l=0; l<num; l++)
	{
		arr[i][j] = cnt;
		cnt++;
		i--;
	}
	i++;	//restoring i--;

	//process4
	j++;	//for next filling
	for(l=0; l<num; l++)	//filling the first row
	{
		arr[i][j] = cnt;
		cnt++;	//save the progress
		j++;
	}
	j--;	//(restroing j++) 

	///////////////
	}//while end
	//////////////
		
	
	printArr(orignal_num, arr);
}

void printArr(int num, int (*arr)[num])
{
	for(int i=0; i<num; i++)
	{
		for(int j=0; j<num; j++)
			printf("%2d ", arr[i][j]);

		printf("\n");
	}
	printf("\n");
}

