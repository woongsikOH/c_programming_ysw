#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void pickDiff3Num(int *, int*, int*);
int areTheyMatched(int, int, int, int, int, int, int *);

int main()
{
	srand((unsigned int)time(NULL));

	//com number
	int n1=0, n2=0, n3=0;

	//user number
	int num1=0, num2=0, num3=0;

	//game arguments
	int try=1;	//number of trying
	int strike=0;	//number of strike

	//computer picks 3 diffrent number
	pickDiff3Num(&n1, &n2, &n3);

	//Baseball game start!
	printf("Base Ball game is started!\n\n");

	while(1)
	{
		printf("try %2d\n", try);
		printf("Input 3 0~9 integer with space : ");
		scanf("%d %d %d", &num1, &num2, &num3);

		if(num1<0 || num2<0 || num3<0 || num1>9 || num2>9 || num3>9)
		{
			printf("Wrong number. The number must be 0~9 integer.\n\n");
			continue;
		}

		//find how many strikes and balls are.
		strike=areTheyMatched(n1, n2, n3, num1, num2, num3, &try);
	
		if(strike == 3)
			break;
	}


	return 0;
}
	
void pickDiff3Num(int *n1, int *n2, int *n3)
{
	//pick 1st number
	*n1=rand()%10;
	//pick 2nd number
	do{
		*n2=rand()%10;
	}while(*n2 == *n1);
	//pick 3rd number
	do{
		*n3=rand()%10;
	}while(*n3 == *n1 || *n3 == *n2);
}

int areTheyMatched(int n1, int n2, int n3, int num1, int num2, int num3, int *try) 
{
	int strike=0, ball=0;

	//check strikes and balls
	if(n1 == num1)
		strike++;
	else if(n1 == num2 || n1 == num3)
		ball++;

	if(n2 == num2)
		strike++;
	else if(n2 == num1 || n2 == num3)
		ball++;

	if(n3 == num3)
		strike++;
	else if(n3 == num1 || n3 == num2)
		ball++;

	//print the result
	if(strike == 3)	
	{
		printf("\nCongratulation !!! You won!!! \n\n");
	}
	else
	{
		printf("Strike : %d Ball : %d \n\n", strike, ball);
		(*try)++;
	}

	return strike;
}
