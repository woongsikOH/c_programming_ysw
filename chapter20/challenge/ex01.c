#include <stdio.h>

void rotateArr(int (*)[], int rows);
void printArr(int (*)[], int rows);

int main()
{
	int arr[4][4]={
		{1,2,3,4},
		{5,6,7,8},
		{9,10,11,12},
		{13,14,15,16}
	};

	printArr(arr, sizeof(arr)/sizeof(arr[0]));

	rotateArr(arr, sizeof(arr)/sizeof(arr[0]));
	printArr(arr, sizeof(arr)/sizeof(arr[0]));

	rotateArr(arr, sizeof(arr)/sizeof(arr[0]));
	printArr(arr, sizeof(arr)/sizeof(arr[0]));

	rotateArr(arr, sizeof(arr)/sizeof(arr[0]));
	printArr(arr, sizeof(arr)/sizeof(arr[0]));

	rotateArr(arr, sizeof(arr)/sizeof(arr[0]));
	printArr(arr, sizeof(arr)/sizeof(arr[0]));

	return 0;
}

void rotateArr(int (*arr)[4], int rows)
{
	int temp[4][4]={0};

	for(int i=0; i<rows; i++)
		for(int j=0; j<4; j++)
			temp[i][j]=arr[3-j][i];	//othre clockwise : temp[i][j]=arr[j][3-i]


	for(int i=0; i<rows; i++)
		for(int j=0; j<4; j++)
			arr[i][j]=temp[i][j];
}	

void printArr(int (*arr)[4], int rows)
{
	for(int i=0; i<rows; i++)
	{
		for(int j=0; j<4; j++)
			printf("%2d ", arr[i][j]);
		printf("\n");
	}

	printf("\n");
}
