#include <stdio.h>

int main()
{
	printf("%-8s %14s %5s \n", "name", "major", "grade");
	printf("%-8s %14s %5s \n", "david", "computer", "3");
	printf("%-8s %14s %5s \n", "sheldon", "physics", "2");
	printf("%-8s %14s %5s \n", "penny", "mathmatics", "4");

	return 0;
}
