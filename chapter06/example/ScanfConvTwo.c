#include <stdio.h>

int main()
{
	float num1;
	double num2;
	long double num3;

	printf("input real number(in 'e format') : ");
	scanf("%f", &num1);
	printf("the input number : %f \n", num1);
	printf("\n");

	printf("input real number(in 'e format') : ");
	scanf("%lf", &num2);
	printf("the input number : %f \n", num2);
	printf("\n");

	printf("input real number(in 'e format') : ");
	scanf("%Lf", &num3);
	printf("the input number : %Lf \n", num3);

	return 0;
}
