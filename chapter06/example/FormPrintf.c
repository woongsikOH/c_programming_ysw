#include <stdio.h>

int main()
{
	const int myAge = 27;
	printf("I'm %d in decimal and %X in hexadecimal\n", myAge, myAge);

	return 0;
}
