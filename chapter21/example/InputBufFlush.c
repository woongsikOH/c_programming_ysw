#include <stdio.h>

void clearLineFromReadBuffer()
{
	while(getchar()!='\n');
}

void clearStdinBufferSafely(char *str)
{
	for(int i=0; i<sizeof(str); i++)
	{
		if(str[i]=='\n')
			return;	
	}

	while(getchar()!='\n');
}


int main()
{
	char perID[7];
	char name[10];

	fputs("Enter SSID first 6 numbers : ", stdout);
	fgets(perID, sizeof(perID), stdin);
	//clearLineFromReadBuffer();
	clearStdinBufferSafely(perID);

	fputs("Enter your name : ", stdout);
	fgets(name, sizeof(perID), stdin);
	//clearLineFromReadBuffer();
	clearStdinBufferSafely(name);

	printf("SSID : %s \n", perID);
	printf("Name : %s \n", name);

	return 0;
}
