#include <stdio.h>
#include <string.h>

int main()
{
	char str1[20];
	char str2[20];

	printf("Enter string 1 : ");
	fgets(str1, sizeof(str1), stdin);

	printf("Enter string 2 : ");
	fgets(str2, sizeof(str2), stdin);

	if(!strcmp(str1, str2))
		printf("Both strings are same! \n");

	else
	{
		printf("Two strings are different! \n");

		if(!strncmp(str1, str2, 3))
			puts("But the first 3 characters are same!!");
	}

	return 0;
}


