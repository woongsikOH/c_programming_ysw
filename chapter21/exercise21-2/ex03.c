#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeWn(char *);
void clearStdin();
void getNameAndAge(char *, char *, int *);


int main()
{
	char str1[50], str2[50];
	char name1[50], name2[50];
	int age1=0, age2=0;

	fputs("Enter first peron's info in this form \'NAME AGE\' : ", stdout);
	fgets(str1, sizeof(str1), stdin);
	removeWn(str1);

	fputs("Enter second peron's info in this form \'NAME AGE\' : ", stdout);
	fgets(str2, sizeof(str2), stdin);
	removeWn(str2);

	getNameAndAge(str1, name1, &age1);
	getNameAndAge(str2, name2, &age2);

	if(!strcmp(name1, name2))
		puts("Both names are same!");
	else
		puts("The names are different!");

	
	if(age1 == age2)
		puts("Ages are same!");
	else
		puts("Ages are different!");



	return 0;
}


void removeWn(char *str)
{
	int leng = strlen(str);

	if(str[leng-1] == '\n')
		str[leng-1] = 0;
	else
		clearStdin();
}

void clearStdin()
{
	while(getchar()!='\n');
}

void getNameAndAge(char *str, char *name, int *age)
{
	int index_space=0;

	//get name
	for(int i=0; i<strlen(str); i++)
	{
		if(str[i] != ' ')
			name[i]=str[i];
		else
		{
			//if find space, then break and save it to int 'index_space';
			index_space=i;
			break;
		}
	}

	//get age
	*age = atoi(&str[index_space+1]);
}
