#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void removeEnter(char *);
void clearStdinBuffer(char *);


int main()
{
	char str[10];
	int sum=0;

	//scanf("%s", str);
	/*	띄어쓰기나 개행문자전까지만 입력 받음. 깔끔함.
	 *	하지만 위의 특징이 단점으로도 작용함.
	 */
	 
	printf("Enter a string : ");
	fgets(str, sizeof(str), stdin);
	clearStdinBuffer(str);

	for(int i=0; i<strlen(str); i++)
	{
		if(str[i] >= 48 && str[i] <= 57)
		{
			if(i != 0 && str[i-1] >= 48 && str[i-1] <= 57)
				continue;

			printf("%c -> %d \n", str[i], atoi(&str[i]));
			sum += atoi(&str[i]);	
		}
	}

	printf("Sum : %d \n", sum);

	return 0;
}

//구현에 굳이 필요하진 않지만 그냥 추가로
void removeEnter(char *str)
{
	int length=strlen(str);
	
	if(str[length-1]='\n')
		str[length-1]=0;
}	

//마찬가지로 필요하진 않지만 복습을 위해
void clearStdinBuffer(char *str)
{
	int length = strlen(str);
	
	if(str[length-1] == '\n')
		str[length-1] = 0;
	else
		while(getchar()!='\n');

}
			
