#include <stdio.h>
#include <string.h>

void clearStdinBuffer(char *);

int main()
{
	char str1[20];
	char str2[20];
	char str3[20];

	fputs("Enter a string1 : ", stdout);
	fgets(str1, sizeof(str1), stdin);
	clearStdinBuffer(str1);

	fputs("Enter a string2 : ", stdout);
	fgets(str2, sizeof(str2), stdin);
	clearStdinBuffer(str2);

	strncpy(str3, str1, sizeof(str3));
	//str3[sizeof(str3)-1]=0;

	//strncat(str3, str2, sizeof(str3)-strlen(str3));
	strncat(str3, str2, 10);
	//str3[sizeof(str3)-1]=0;

	puts(str3);

	
	return 0;
}

void clearStdinBuffer(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
	{
		str[length-1] = 0;
	}
	else
		while(getchar()!='\n');
}
