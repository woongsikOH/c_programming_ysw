#include <stdio.h>
#include <string.h>

void removeWn(char *);

int main()
{
	/*
	 * test for whether strlen includes '\0' or not
	 * conclusion : excluding '\0'
	 *
	char test[5]="01234";
	test[4]=0;
	printf("%lu \n", strlen(test));
	*
	*/

	char str_fgets[10];

	fgets(str_fgets, sizeof(str_fgets), stdin);
	
	putchar('\n');
	puts("Before remove \'\\n\'");
	fputs(str_fgets, stdout);

	removeWn(str_fgets);
	puts("Ater remove \'\\n\'");
	fputs(str_fgets, stdout);


	return 0;
}


void removeWn(char *str)
{
	int length = strlen(str);

	if(str[length-1] == '\n')
		str[length-1] = 0;
}
