#include <stdio.h>

void simpleWipeStdin()
{
	while(getchar()!='\n');
}

int main()
{
	/*
	 * puts() and fputs()
	 */

	/*
	 *
	char *str_puts;
	int int_puts;

	char *str_fputs="It's a test motherfucker!\n";
	int int_fputs;

	int_puts=puts("test string");
	int_fputs=fputs(str_fputs, stdout);

	putchar('\n');
	printf("The return value of puts() : %d \n", int_puts);
	printf("The return value of fputs() : %d \n", int_fputs);
	putchar('\n');

	*/


	/*
	 * gets and fgets()
	 */

	//gets is recommanded not to use.
	/*
	char str[7];
	gets(str);
	fputs("The result of gets : ", stdout), puts(str); 
	*/
	char str_fgets[7];

	for(int i=0; i<3; i++)
	{
	fgets(str_fgets, sizeof(str_fgets), stdin);
	puts(str_fgets);
	}
	


	return 0;
}
