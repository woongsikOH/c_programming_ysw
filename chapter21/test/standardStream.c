#include <stdio.h>

void simpleWipeStdin()
{
	while(getchar()!='\n');
}

int main()
{
	//putchar() and fputc()
	int int_putchar=0;
	int int_fputc=0;
	

	/*
	 * putchar() and fputc()
	 */

	int_putchar=putchar(65);
	putchar('\n');

	int_fputc=fputc(66, stdout);
	putchar('\n');

	//print return value;
	putchar('\n');
	printf("putchar()'s return value : %d \n", int_putchar);
	printf("fputc()'s return value : %d \n", int_fputc);



	/*
	 * getchar() and fgetc()
	 */

	int int_getchar;
	int int_fgetc;

	//getchar()
	putchar('\n');

	fputs("Enter a character : ", stdout);
	int_getchar=getchar();

	//개행 문자 나올때까지 stdin에서 문자 빼내기
	simpleWipeStdin();


	//fgetc()
	//
	fputs("Enter a character : ", stdout);
	int_fgetc=fgetc(stdin);

	//개행 문자 나올때까지 stdin에서 문자 빼내기
	simpleWipeStdin();


	//print the results
	putchar('\n');
	printf("The result of getchar's int variable : %c \n", int_getchar);
	printf("The result of fputc's int variable : %c \n", int_fgetc);
	putchar('\n');


	/*
	 * puts() and fputs()
	 */

	char *str_puts;
	int int_puts;

	char *str_fputs="It's a test motherfucker!\n";
	int int_fputs;

	int_puts=puts("test string");
	int_fputs=fputs(str_fputs, stdout);

	putchar('\n');
	printf("The return value of puts() : %d \n", int_puts);
	printf("The return value of fputs() : %d \n", int_fputs);
	putchar('\n');



	return 0;
}
