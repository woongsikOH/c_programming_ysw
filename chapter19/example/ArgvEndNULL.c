#include <stdio.h>

int main(int argc, char *argv[])
{
	int i=0;
	printf("the number of delivered arguments : %d \n", argc);

	while(argv[i]!=NULL)
	{
		printf("String %d : %s \n", i+1, argv[i]);
		i++;
	}

	return 0;
}
