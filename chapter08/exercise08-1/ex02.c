#include <stdio.h>

int main()
{
	int num1=0, num2=0, result=0;

	printf("Enter two integers in decimal : ");
	scanf("%d %d", &num1, &num2);

	result = num1>num2 ? num1-num2:num2-num1;

	printf("Result : %d \n", result);

	return 0;
}
