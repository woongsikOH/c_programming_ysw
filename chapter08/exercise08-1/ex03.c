#include <stdio.h>

int main()
{
	int score1=0, score2=0, score3=0;
	double average=0.0;

	printf("Enter a 3 subjects' score. (i1 i2 i3) : ");
	scanf("%d %d %d", &score1, &score2, &score3);

	average = (double)(score1 + score2 + score3) / 3;

	printf("Average Score : %f \n", average);

	if(average >= 90)
		printf("Your grade is A \n");

	else if(average >= 80)
		printf("Your grade is B \n");

	else if(average >= 70)
		printf("Your grade is C \n");

	else if(average >= 50)
		printf("Your grade is D \n");

	else
		printf("Your grade is F \n");

	return 0;
}

