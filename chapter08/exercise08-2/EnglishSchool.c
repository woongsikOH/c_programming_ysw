#include <stdio.h>

int main()
{
	int num;
	int condition;

	while(1){

		printf("Enter a integer between 1 and 5 : ");
		scanf("%d", &num);

		condition = (num>=1 && num<=5) ? 1:0;

		if(condition)
			break;
	}

	switch(num)
	{
		case 1:
			printf("1 is ONE \n");
			break;

		case 2:
			printf("2 is TWO \n");
			break;

		case 3:
			printf("3 is THREE \n");
			break;

		case 4:
			printf("4 is FOUR \n");
			break;

		case 5:
			printf("5 is FIVE \n");
			break;

		default:
			printf("Unexpexted ERROR! \n");
	}

	return 0;
}
