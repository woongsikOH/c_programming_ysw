#include <stdio.h>

int main()
{
	int num=0;

	printf("Enter a positive integer : ");
	scanf("%d", &num);

	//num %= 10;

	switch(num/10)
	{
		case 0:
			printf("0이상 10미만 \n");
			break;

		case 1:
			printf("10이상 20미만 \n");
			break;

		case 2:
			printf("20이상 30미만 \n");
			break;

		default:
			printf("30 이상 \n");
	}

	return 0;
}
