#include <stdio.h>

int main()
{
	int num;
	printf("Enter a decimal integer : ");
	scanf("%d", &num);

	if(num < 0)
		printf("The input number is less than 0. \n");
	else
		printf("The input number is not less than 0. \n");

	return 0;
}
