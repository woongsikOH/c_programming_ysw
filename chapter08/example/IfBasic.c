#include <stdio.h>

int main()
{
	int num;
	printf("Enter a decimal integer : ");
	scanf("%d", &num);

	if(num < 0)
		printf("The input number is smaller than 0.\n");

	if(num > 0)
		printf("The input number is bigger than 0.\n");

	if(num == 0)
		printf("The input number is 0. \n");

	return 0;
}

