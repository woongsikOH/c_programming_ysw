#include <stdio.h>

int main()
{
	int num, abs;
	printf("Enter a decimal integer : ");
	scanf("%d", &num);

	abs = num>0 ? num : num*(-1);

	printf("Absolute Num : %d \n", abs);

	return 0;
}
