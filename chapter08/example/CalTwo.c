#include <stdio.h>

int main()
{
	int opt;
	double num1, num2, result;

	printf("1.Addition 2.Subtraction 3.Mulplication 4.Division \n");
	printf("Choose one : ");
	scanf("%d", &opt);

	printf("Enter two real number (r1 r2) : ");
	scanf("%lf %lf", &num1, &num2);

	if(opt == 1)
		result = num1 + num2;

	else if(opt == 2)
		result = num1 - num2;

	else if(opt == 3)
		result = num1 * num2;

	else
		result = num1 / num2;

	printf("Result : %f \n", result);

	return 0;
}
