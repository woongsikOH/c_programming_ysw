#include <stdio.h>

int main()
{
	int num;
	printf("Enter a natural number : ");
	scanf("%d", &num);

	if(num==1)
		goto ONE;
	else if(num==2)
		goto TWO;
	else
		goto OTHER;

ONE:
	printf("You typed a '1'.\n");
	goto END;

TWO:
	printf("You typed a '2'.\n");
	goto END;

OTHER:
	printf("You typed a '3' or ohter number.\n");
	
END:
	return 0;
}
