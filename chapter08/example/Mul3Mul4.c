#include <stdio.h>

int main()
{
	for(int num=1; num<100; num++){
		
		if(num%3==0 || num%4==0){
			printf("%d은(는) 3 또는 4의 배수 입니다.\n", num);
		}
	}

	return 0;
}
