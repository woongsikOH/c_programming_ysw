#include <stdio.h>

int squareByValue(int);
int squareByReference(int *);

int main()
{
	int num=7;

	printf("num : %d \n", num);
	printf("square by value : %d \n", squareByValue(num));
	
	printf("num : %d \n", num);
	printf("square by reference : %d \n", squareByReference(&num));

	printf("num : %d \n", num);

	return 0;
}

int squareByValue(int num)
{
	num*=num;
	return num;
}

int squareByReference(int *num)
{
	*num= (*num)*(*num);
	return *num;
}
