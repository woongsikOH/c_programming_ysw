#include <stdio.h>

void swap3(int *, int *, int *);

int main()
{
	int num1=1, num2=2, num3=3;

	printf("num1 num2 num3 : %d %d %d \n", num1, num2, num3);

	swap3(&num1, &num2, &num3);
	printf("Ater swap \n");
	printf("num1 num2 num3 : %d %d %d \n", num1, num2, num3);

	return 0;
}

void swap3(int *num1, int *num2, int *num3)
{
	int temp=0;

	temp = *num3;
	*num3 = *num2;
	*num2 = *num1;
	*num1 = temp;
}
